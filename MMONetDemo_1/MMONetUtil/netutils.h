#pragma once

#include "stdlib.h"

#include  <iostream>
#include  <string>
#include <atomic>
#include <thread>

#include "WinSock2.h"
#include "windows.h"

#include "ws2tcpip.h"

#include <conio.h>

#define ShowTips printf

const int BUF_SIZE = 1024;


const char* NET_ADDR_C = "192.168.199.200";
const int NET_PORT_C = 5555;

const char* NET_ADDR_S = "192.168.199.200";
const int NET_PORT_S = 5556;


void SystemError()
{
	int error = WSAGetLastError();
	if (error == 0)
	{
		return;
	}

	std::cout << "Last Error : " << error << std::endl;
}

void ReportError(const char* error)
{
	std::cout << "ERROR: " << error << std::endl;
}



sockaddr_in InitAddr(const char* address, const int port)
{
	sockaddr_in addr_in;
	memset(addr_in.sin_zero, 0, sizeof(addr_in.sin_zero));

	addr_in.sin_family = AF_INET;
	addr_in.sin_port = htons(port);
	inet_pton(AF_INET, address, &addr_in.sin_addr);

	return addr_in;
}

