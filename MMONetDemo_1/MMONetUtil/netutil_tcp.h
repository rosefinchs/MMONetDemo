#pragma once

#include "netutils.h"

void SendAndRecv(const SOCKET& tcpsock)
{
	int result = 0;
	char msgBuf[BUF_SIZE] = { 0 };
	while (true)
	{
		if (_kbhit())
		{
			memset(msgBuf, 0, BUF_SIZE);
			std::cout << "Send Message: " << std::ends;
			std::cin >> msgBuf;

			if (std::string("quit") == msgBuf)
			{
				ShowTips("User quit !\n");
				break;
			}

			result = send(tcpsock, msgBuf, strlen(msgBuf), 0);
			if (result < 0)
			{
				ReportError("Send Error");
				continue;
			}
			else
			{
				ShowTips("Send OK !\n");
			}
		}

		//=======================recv=====================
		memset(msgBuf, 0, BUF_SIZE);
		result = recv(tcpsock, msgBuf, BUF_SIZE, 0);
		if (result > 0)
		{
			ShowTips("Recv Message: %s\n", msgBuf);
		}

		Sleep(10);
	}
}


void TCP_Protocol_Client(const char* serverIP, const int serverPort, const char* clientIP, const int clientPort)
{
	sockaddr_in addr_client = InitAddr(clientIP, clientPort);
	sockaddr* pAddrClient = (sockaddr*)(&addr_client);
	sockaddr_in addr_server = InitAddr(serverIP, serverPort);
	sockaddr* pAddrServer = (sockaddr*)(&addr_server);

	if (!pAddrClient || !pAddrServer)
	{
		return ReportError("Init Address Error");
	}

	// 创建一个流式套接字（TCP）
	SOCKET tcpsock = socket(AF_INET, SOCK_STREAM, 0);
	if (tcpsock == INVALID_SOCKET)
	{
		return ReportError("Create Socket error");
	}

	// 绑定到本地地址和端口
	if (bind(tcpsock, pAddrClient, sizeof(sockaddr)) < 0)
	{
		closesocket(tcpsock);
		return ReportError("Bind Socket error");
	}

	// 主动连接服务器
	int result = connect(tcpsock, pAddrServer, sizeof(sockaddr));
	if (result == SOCKET_ERROR)
	{
		closesocket(tcpsock);
		return ReportError("Connect Error");
	}

	ShowTips("Connect Server OK\n");

	// 设置socket读写为非阻塞模式
	u_long arg = 1;
	result = ioctlsocket(tcpsock, FIONBIO, &arg);
	if (result == SOCKET_ERROR)
	{
		closesocket(tcpsock);
		return ReportError("Set NonBlock error");
	}

	// 收发数据
	SendAndRecv(tcpsock);

	// 停止收发数据
	shutdown(tcpsock, SD_BOTH);
	// 释放socket
	closesocket(tcpsock);
}


void TCP_Protocol_Server(const char* serverIP, const int serverPort)
{
	sockaddr_in addr_client = InitAddr(NET_ADDR_C, NET_PORT_C);
	sockaddr* pAddrClient = (sockaddr*)(&addr_client);
	sockaddr_in addr_server = InitAddr(serverIP, serverPort);
	sockaddr* pAddrServer = (sockaddr*)(&addr_server);

	if (!pAddrClient || !pAddrServer)
	{
		return ReportError("Init Address Error");
	}

	// 监听socket
	SOCKET tcpsock_listen = socket(AF_INET, SOCK_STREAM, 0);
	if (tcpsock_listen == INVALID_SOCKET)
	{
		return ReportError("Create Socket error");
	}

	// 绑定本机的一个地址和端口
	if (bind(tcpsock_listen, pAddrServer, sizeof(sockaddr)) < 0)
	{
		closesocket(tcpsock_listen);
		return ReportError("Bind Socket error");
	}

	// 开始监听（最多能接受SOMAXCONN个连接请求）
	int result = listen(tcpsock_listen, SOMAXCONN);
	if (result == SOCKET_ERROR)
	{
		closesocket(tcpsock_listen);
		return ReportError("Listen error");
	}


	ShowTips("Server is Listening...\n");


	SOCKET sockClient = INVALID_SOCKET;
	while (true)
	{
		// 监听到一个客户端连接请求，接受连接
		int clientAddrLen = sizeof(sockaddr);
		sockClient = accept(tcpsock_listen, pAddrClient, &clientAddrLen);
		if (sockClient == INVALID_SOCKET)
		{
			continue;
		}
		else
		{
			int port = ntohs(addr_client.sin_port);
			char addrBuf[128] = { 0 };
			inet_ntop(AF_INET, &addr_client.sin_addr, addrBuf, 128);

			ShowTips("Client Connected! (%s)\n", addrBuf);

			break;
		}

		Sleep(10);
	}

	if (sockClient == INVALID_SOCKET)
	{
		closesocket(tcpsock_listen);
		return ReportError("Accept Error");
	}

	// 设置socket读写为非阻塞模式
	u_long arg = 1;
	result = ioctlsocket(sockClient, FIONBIO, &arg);
	if (result == SOCKET_ERROR)
	{
		closesocket(tcpsock_listen);
		closesocket(sockClient);
		return ReportError("Set NonBlock error");
	}

	// 收发数据
	SendAndRecv(sockClient);

	// 停止收发数据
	shutdown(tcpsock_listen, SD_BOTH);
	shutdown(sockClient, SD_BOTH);

	// 释放socket
	closesocket(tcpsock_listen);
	closesocket(sockClient);
}