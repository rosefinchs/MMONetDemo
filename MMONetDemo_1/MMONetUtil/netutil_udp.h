#pragma once

#include "netutils.h"


void UDP_Protocol(const char* hereIP, const int herePort, const char* thereIP, const int therePort)
{
	sockaddr_in addr_here = InitAddr(hereIP, herePort);
	sockaddr* pAddrHere = (sockaddr*)(&addr_here);
	sockaddr_in addr_s = InitAddr(thereIP, therePort);
	sockaddr* pAddrThere = (sockaddr*)(&addr_s);

	sockaddr_in addr_from = InitAddr(hereIP, herePort);
	sockaddr* pAddrFrom = (sockaddr*)&addr_from;
	int addr_len = sizeof(sockaddr);

	if (!pAddrThere || !pAddrHere)
	{
		return ReportError("Address error");
	}

	SOCKET udpsock = socket(AF_INET, SOCK_DGRAM, 0);
	if (udpsock == INVALID_SOCKET)
	{
		return ReportError("Create Socket error");
	}

	if (bind(udpsock, pAddrHere, sizeof(sockaddr)) < 0)
	{
		closesocket(udpsock);
		return ReportError("Bind Error");
	}


	// 设置socket读写为非阻塞模式
	u_long arg = 1;
	int result = ioctlsocket(udpsock, FIONBIO, &arg);
	if (result == SOCKET_ERROR)
	{
		closesocket(udpsock);
		return ReportError("Set NonBlock error");
	}

	char msgBuf[BUF_SIZE];
	while (1)
	{
		//SystemError();
		//=======================send======================
		if (_kbhit())
		{
			memset(msgBuf, 0, BUF_SIZE);
			std::cout << "Send Message: " << std::ends;
			std::cin >> msgBuf;

			if (std::string("quit") == msgBuf)
			{
				break;
			}

			result = sendto(udpsock, msgBuf, BUF_SIZE, 0, pAddrThere, sizeof(sockaddr));
			if (result < 0)
			{
				ReportError("Send Error");
				continue;
			}
			else
			{
				std::cout << "Send OK !" << std::endl;
			}
		}

		//=======================recv=====================
		memset(msgBuf, 0, BUF_SIZE);
		result = recvfrom(udpsock, msgBuf, BUF_SIZE, 0, pAddrFrom, &addr_len);
		if (result > 0)
		{
			int port = ntohs(addr_from.sin_port);
			char addrBuf[128] = { 0 };
			inet_ntop(AF_INET, &addr_from.sin_addr, addrBuf, 128);

			std::cout << "Recv Message(" << addrBuf << ":" << port << "): " << std::ends;
			std::cout << msgBuf << std::endl;
		}

		Sleep(10);
	}

	shutdown(udpsock, SD_BOTH);
	closesocket(udpsock);
}
